Pod::Spec.new do |s|
  s.name          = "MSViewPagerController"
  s.version       = "1.0.1"
  s.summary       = "(Swift 3 only) A ViewPager using UICollectionView to mimick Android behaviour."
  s.homepage      = "http://myshopi.com"
  s.license = { :type => "MIT", :file => "LICENSE" }
  s.author        = { "Agilys" => "ff@agilys.be" }
  s.source        = { :git => "https://fredfauquette@bitbucket.org/fredfauquette/msviewpagercontroller.git", :tag =>  "#{s.version}" }
  s.ios.deployment_target = "9.0"
  s.source_files  = "MSViewPagerController/MSViewPagerController/Sources/*.swift"
  s.requires_arc  = true
  s.ios.resource_bundle = { 'MSViewPagerController' => 'MSViewPagerController/MSViewPagerController/Sources/Resources/*.storyboard' }
end
