//
//  MSViewPagerController.swift
//  MSViewPagerController
//
//  Created by fauquette fred on 5/07/16.
//  Copyright © 2016 Agilys. All rights reserved.
//

import UIKit

/**
 a public enum for errors
 
 - IndexOutOfBound:      Index is out of bounds
 - IndexAlreadySelected: Index is already selected
 */
public enum MSViewPagerError: Error {
    case indexOutOfBound
    case indexAlreadySelected
}

@objc public enum MSViewPagerState: Int, Hashable {
    case normal = 0
    case selected = 1
    
    public var hashValue: Int {
        return rawValue
    }
}

public func ==(lhs: MSViewPagerState, rhs: MSViewPagerState) -> Bool {
    return lhs.hashValue == rhs.hashValue
}

/// a protocol for the model object
@objc public protocol MSPageModelProtocol {
    /// the title of the cell
    var title: String? {get}
    /// the icon of the cell
    var icon: UIImage? {get}
}

/// a protocol for the delegate of the ViewPager
@objc public protocol MSViewPagerDelegate {
    /**
     triggered when a cell is selected, not triggered if cell is already selected
     
     - parameter viewPager: the source viewPager
     - parameter index:     the index of the selected cell
     */
    func didSelectMSPage(_ viewPager: MSViewPagerProtocol, index: Int)
    
    /**
     an optional method to get the TextColor for a specific index (called when cell is set)
     
     - parameter index: index
     - parameter state: the state
     
     - returns: a potential color. If nil then global colors are used
     */
    @objc optional func getTextColorMSPage(_ index: Int, state: UIControlState) -> UIColor?
    
    /**
     an optional method to get the BackGroundColor for a specific index (called when cell is set)
     
     - parameter index: index
     - parameter state: the state
     
     - returns: a potential color. If nil then global colors are used
     */
    @objc optional func getBackGroundColorMSPage(_ index: Int, state: UIControlState) -> UIColor?
    
    /**
     an optional method to get the BarColor for a specific index (called when cell is set)
     
     - parameter index: index
     - parameter state: the state
     
     - returns: a potential color. If nil then global colors are used
     */
    @objc optional func getBarColorMSPage(_ index: Int) -> UIColor?
}

/// a protocol the datasource of the ViewPager
@objc public protocol MSViewPagerDataSource {
    /**
     to get the model associated with the index
     
     - parameter index: the index (index based on zero with max numberOfPagesMSPage -1)
     
     - returns: the model or nil if no model for index
     */
    func pageModelMSPage(_ index: Int) -> MSPageModelProtocol?
    
    /// the number of pages
    var numberOfPagesMSPage: Int {get}
}

/// the protocol for ViewPager
@objc public protocol MSViewPagerProtocol: class {
        /// the delegate of the ViewPager
    var pagerDelegate: MSViewPagerDelegate? {get set}
        /// the dataSource of the ViewPager
    var dataSource: MSViewPagerDataSource? {get set}
        /// the color of the bar
    var barColor:UIColor {get set}
        /// the font used for the labels
    var fontUsed: UIFont {get set}
        /// the minimal width of a cell
    var minimalWidth: CGFloat {get set}
        /// the leading and trailing space for a label (with it's cell)
    var horizontalTextSpacing: CGFloat {get set}
        /// the height of the bar
    var barHeight: CGFloat {get set}
    
    /**
     set the global colors for textColors
     
     - parameter color: the colors
     - parameter state: the specific state
     */
    func setTextColor(_ color: UIColor, state: MSViewPagerState)
    
    /**
     set the global colors for backgroundColors
     
     - parameter color: the colors
     - parameter state: the specific state
     */
    func setBackGroundColor(_ color: UIColor, state: MSViewPagerState)
    
    /**
     reload all the data
     */
    func reloadata()
    
    /**
     set the selected index to the specific value. Throw an error if index is out of bounds or if index already selected
     
     - parameter selectedIndex: the new selected index (index based zero)
     - parameter animated:      a boolean
     */
    func setSelectedIndex(_ selectedIndex: Int, animated: Bool) throws
}


//MARK: - MSViewPagerController
/// the viewPagerController class
@objc open class MSViewPagerController: UIViewController {
    //MARK: - public vars
    open var pagerDelegate: MSViewPagerDelegate?
    open var dataSource: MSViewPagerDataSource?
    
    open var barColor = UIColor.black
    open var fontUsed = UIFont.systemFont(ofSize: 13)
    open var minimalWidth: CGFloat = 50
    open var horizontalTextSpacing: CGFloat = 4
    open var barHeight: CGFloat = 5
    
    //MARK: - private vars
    fileprivate var selectedIndex: Int = 0
    fileprivate var textColors: [MSViewPagerState: UIColor] = [.normal : UIColor.lightGray,
                                                           .selected : UIColor.black]
    fileprivate var backgroundColors: [MSViewPagerState: UIColor] = [.normal : UIColor.white,
                                                                 .selected : UIColor.white]
    fileprivate var numberOfItems = 0
    
    fileprivate static let ReuseIdentifier = "MSViewPagerCell"
    
    
    @IBOutlet fileprivate weak var barView: UIView!
    @IBOutlet fileprivate weak var collectionView: UICollectionView!
    @IBOutlet fileprivate weak var barHeightConstraint: NSLayoutConstraint!
    @IBOutlet fileprivate weak var barWidthConstraint: NSLayoutConstraint!
    @IBOutlet fileprivate weak var barXOffsetConstraint: NSLayoutConstraint!
    
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        numberOfItems = dataSource?.numberOfPagesMSPage ?? 0
        if let tmpLayout = collectionView?.collectionViewLayout as? MSViewPagerControllerFlowLayout {
            tmpLayout.layoutDelegate = self
        }
        barHeightConstraint.constant = barHeight
        barView.backgroundColor = barColor
        collectionView.backgroundColor = backgroundColors[.selected]
        moveBar(false){[unowned self] in self.collectionView.reloadData() }
        
        
    }
    
    fileprivate func selectIndex(_ index: Int, force: Bool = false) {
        if index != selectedIndex || force {
            if let oldCell = collectionView.cellForItem(at: IndexPath(row: selectedIndex, section: 0)) as? MSViewPagerCollectionViewCell , index != selectedIndex {
                oldCell.selectCell(false)
            }
            let nextIndexPath = IndexPath(row: index, section: 0)
            if let nextCell = collectionView.cellForItem(at: nextIndexPath) as? MSViewPagerCollectionViewCell{
                nextCell.selectCell(true)
            }
            selectedIndex = index
            moveBar()
        }
    }
    
    fileprivate func moveBar(_ animated: Bool = true, withScroll: Bool = true, completion:(() -> ())? = nil) {
        let nextIndexPath = IndexPath(row: selectedIndex, section: 0)
        var newBarColor = barColor
        if let pagerDelegate = pagerDelegate {
            newBarColor = pagerDelegate.getBarColorMSPage?(selectedIndex) ?? barColor
        }
        if let frame = collectionView.layoutAttributesForItem(at: nextIndexPath)?.frame {
            if animated {
                UIView.animate(withDuration: 0.3,
                                           delay: 0,
                                           options: .curveEaseOut,
                                           animations: {                                            
                                            self.barView.backgroundColor = newBarColor
                                            self.barWidthConstraint.constant = frame.width
                                            self.barXOffsetConstraint.constant = frame.origin.x - self.collectionView.contentOffset.x
                                            if withScroll {
                                                self.collectionView.contentOffset = CGPoint(x: max(0,min(frame.origin.x + frame.width / 2 - self.collectionView.bounds.width / 2, self.collectionView.contentSize.width - self.collectionView.bounds.width)) , y: 0)
                                            }
                                            self.view.layoutIfNeeded()
                    },
                                           completion: { (completed) in
                                            if let completion = completion {
                                                completion()
                                            }
                })
            } else {
                barWidthConstraint.constant = frame.width
                barXOffsetConstraint.constant = frame.origin.x - self.collectionView.contentOffset.x
                if withScroll {
                    self.collectionView.contentOffset = CGPoint(x: max(0,min(frame.origin.x + frame.width / 2 - self.collectionView.bounds.width / 2, self.collectionView.contentSize.width - self.collectionView.bounds.width)) , y: 0)
                }
                if let completion = completion {
                    completion()
                }
            }
        }
    }
    
}

//MARK: - MSViewPagerProtocol
extension MSViewPagerController: MSViewPagerProtocol {
    public func setTextColor(_ color: UIColor, state: MSViewPagerState) {
        textColors[state] = color
    }
    public func setBackGroundColor(_ color: UIColor, state: MSViewPagerState) {
        backgroundColors[state] = color
    }
    public func reloadata() {
        numberOfItems = dataSource?.numberOfPagesMSPage ?? 0
        collectionView.reloadData()
        moveBar()
    }
    public func setSelectedIndex(_ nextIndex: Int, animated: Bool) throws {
        guard nextIndex >= 0 else {
            throw MSViewPagerError.indexOutOfBound
        }
        guard nextIndex < numberOfItems else {
            throw MSViewPagerError.indexOutOfBound
        }
        guard nextIndex != selectedIndex else {
            throw MSViewPagerError.indexAlreadySelected
        }
        selectIndex(nextIndex)
    }
}

//MARK: - UIScrollViewDelegate

extension MSViewPagerController: UIScrollViewDelegate {
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        moveBar(withScroll: false)
    }
}

//MARK: - UICollectionViewDelegate
extension MSViewPagerController: UICollectionViewDelegate {
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        pagerDelegate?.didSelectMSPage(self, index: (indexPath as NSIndexPath).row)
        selectIndex((indexPath as NSIndexPath).row)
    }
}

//MARK: - UICollectionViewDataSource
extension MSViewPagerController: UICollectionViewDataSource {
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return numberOfItems
    }
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MSViewPagerController.ReuseIdentifier, for: indexPath) as! MSViewPagerCollectionViewCell
        cell.setupWithModel(dataSource?.pageModelMSPage((indexPath as NSIndexPath).row))
        cell.setupColors(getTextColors((indexPath as NSIndexPath).row), backgroundColors: getBackGroundColors((indexPath as NSIndexPath).row))
        cell.setUpFont(fontUsed)
        cell.selectCell((indexPath as NSIndexPath).row == selectedIndex)
        return cell
    }
    
    fileprivate func getTextColors(_ index: Int) -> [MSViewPagerState : UIColor] {
        guard let pagerDelegate = pagerDelegate else {
            return textColors
        }
        var tmpColors = [MSViewPagerState : UIColor]()
        tmpColors[.selected] = pagerDelegate.getTextColorMSPage?(index, state: .selected) ?? textColors[.selected]
        tmpColors[.normal] = pagerDelegate.getTextColorMSPage?(index, state: .selected) ?? textColors[.normal]
        return tmpColors
    }
    
    fileprivate func getBackGroundColors(_ index: Int) -> [MSViewPagerState : UIColor] {
        guard let pagerDelegate = pagerDelegate else {
            return backgroundColors
        }
        var tmpColors = [MSViewPagerState : UIColor]()
        tmpColors[.selected] = pagerDelegate.getBackGroundColorMSPage?(index, state: .selected) ?? backgroundColors[.selected]
        tmpColors[.normal] = pagerDelegate.getBackGroundColorMSPage?(index, state: .selected) ?? backgroundColors[.normal]
        return tmpColors
    }
}

//MARK: - MSViewPagerControllerFlowLayoutDelegate
extension MSViewPagerController: MSViewPagerControllerFlowLayoutDelegate {
    func pageModelMSPage(_ index: Int) -> MSPageModelProtocol? {
        return dataSource?.pageModelMSPage(index)
    }
}

//MARK: - MSViewPagerCollectionViewCell
/// the cell class
class MSViewPagerCollectionViewCell : UICollectionViewCell {
    @IBOutlet fileprivate weak var mainTitle: UILabel!
    fileprivate var textColors: [MSViewPagerState: UIColor]?
    fileprivate var backgroundColors: [MSViewPagerState: UIColor]?
    
    func setupWithModel(_ model: MSPageModelProtocol?) {
        mainTitle.text = model?.title
    }
    
    func setUpFont(_ font: UIFont) {
        mainTitle.font = font
    }
    
    func setupColors(_ textColors: [MSViewPagerState: UIColor], backgroundColors: [MSViewPagerState: UIColor]) {
        self.textColors = textColors
        self.backgroundColors = backgroundColors
    }
    
    func selectCell(_ selected: Bool) {
        if selected {
            if let bgColor = backgroundColors?[.selected] {
                contentView.backgroundColor = bgColor
            }
            if let textColor = textColors?[.selected] {
                mainTitle.textColor = textColor
            }
        } else {
            if let bgColor = backgroundColors?[.normal] {
                contentView.backgroundColor = bgColor
            }
            if let textColor = textColors?[.normal] {
                mainTitle.textColor = textColor
            }
        }
    }
}

protocol MSViewPagerControllerFlowLayoutDelegate: class {
    func pageModelMSPage(_ index: Int) -> MSPageModelProtocol?
    var fontUsed: UIFont {get}
    var minimalWidth: CGFloat {get}
    var horizontalTextSpacing: CGFloat {get}
}

class MSViewPagerControllerFlowLayout: UICollectionViewLayout {
    fileprivate var layoutAttributes = Dictionary<String, UICollectionViewLayoutAttributes>()
    fileprivate var contentSize = CGSize.zero
    fileprivate lazy var minimalSize: CGSize = {
        return CGSize(width: self.layoutDelegate?.minimalWidth ?? 0, height: self.collectionView!.bounds.height)
    }()
    
    weak var layoutDelegate: MSViewPagerControllerFlowLayoutDelegate?
    
    /**
     let's do the layout here
     */
    override func prepare() {
        super.prepare()
        layoutAttributes = Dictionary<String, UICollectionViewLayoutAttributes>()
        
        var xOffset: CGFloat = 0
        let numberOfItems = collectionView!.numberOfItems(inSection: 0)
        var itemSize = CGSize.zero
        for index in 0 ..< numberOfItems {
            if let model = layoutDelegate?.pageModelMSPage(index), let textWidth = model.title?.size(attributes: [NSFontAttributeName: layoutDelegate?.fontUsed ?? UIFont.systemFont(ofSize: 10)]).width {
                itemSize = CGSize(width: max(textWidth + 2 * (layoutDelegate?.horizontalTextSpacing ?? 0), layoutDelegate?.minimalWidth ?? 0), height: self.collectionView!.bounds.height)
            } else {
                itemSize = minimalSize
            }
            let indexPath = IndexPath(item: index, section: 0)
            let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
            attributes.frame = CGRect(x: xOffset, y: 0, width: itemSize.width, height: itemSize.height).integral
            let key = layoutKeyForIndexPath(indexPath)
            layoutAttributes[key] = attributes
            xOffset += itemSize.width
        }
        
        contentSize = CGSize(width: xOffset, height: collectionView!.bounds.height) // The 20 just adds some extra spacing at the bottom
    }
    
    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return false
    }
    
    override var collectionViewContentSize : CGSize {
        return contentSize
    }
    
    fileprivate func layoutKeyForIndexPath(_ indexPath : IndexPath) -> String {
        return "\((indexPath as NSIndexPath).section)_\((indexPath as NSIndexPath).row)"
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        
        let key = layoutKeyForIndexPath(indexPath)
        if let attributes = layoutAttributes[key] {
            return attributes
        }
        return super.layoutAttributesForItem(at: indexPath)
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        return Array(layoutAttributes.values).filter{rect.intersects($0.frame)}
    }
}
